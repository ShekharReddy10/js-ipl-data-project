function HighestOnePlayerDismissedByAnotherPlayer(deliveriesArray) {
  const batsmanBowlerPair = deliveriesArray
    .filter((delivery) => {
      return (
        delivery.player_dismissed === delivery.batsman &&
        delivery.dismissal_kind !== "run out"
      );
    })
    .map((delivery) => {
      return [delivery.batsman, delivery.bowler];
    });
  const numberOfDismissals = batsmanBowlerPair.reduce((acc, curr) => {
    if (acc[curr] === undefined) {
      acc[curr] = 1;
    } else {
      acc[curr] += 1;
    }
    return acc;
  }, {});
  let highestDismissals = Object.entries(numberOfDismissals).sort(
    ([, name1], [, name2]) => name2 - name1
  );
  return highestDismissals[0];
}
module.exports = HighestOnePlayerDismissedByAnotherPlayer;
