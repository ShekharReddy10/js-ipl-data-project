function matchesWonPerTeamPerYear(matchesArray) {
  let matchesWon = matchesArray.reduce((acc, curr) => {
    let year = curr.season;
    let team = curr.winner;
    if (acc[year] === undefined) {
      acc[year] = { [team]: 1 };
    } else {
      if (acc[year][team] === undefined) {
        acc[year][team] = 1;
      } else {
        acc[year][team] += 1;
      }
    }
    return acc;
  }, {});
  return matchesWon;
}
module.exports = matchesWonPerTeamPerYear;
