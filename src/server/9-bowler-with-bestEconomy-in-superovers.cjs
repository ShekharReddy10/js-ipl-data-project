function bowlerWithBestEconomyInSuperOvers(deliveriesArray) {
  const superOverDeliveries = deliveriesArray.filter((delivery) => {
    if (delivery.is_super_over == 1) {
      return delivery;
    }
  });
  const runsAndBalls = superOverDeliveries.reduce((acc, curr) => {
    let ballCount = 1;
    let bowler = curr.bowler;
    if (curr.wide_runs !== "0" || curr.noball_runs !== "0") {
      ballCount = 0;
    }
    if (acc[bowler] === undefined) {
      acc[bowler] = [parseInt(curr.total_runs), ballCount];
    } else {
      let addRunsAndBalls = acc[bowler];
      acc[bowler] = [
        addRunsAndBalls[0] + parseInt(curr.total_runs),
        addRunsAndBalls[1] + ballCount,
      ];
    }
    return acc;
  }, {});
  const economiesArray = Object.entries(runsAndBalls)
    .map((bowler) => {
      bowler[1] = parseFloat(((bowler[1][0] * 6) / bowler[1][1]).toFixed(2));
      return [bowler[0], bowler[1]];
    })
    .sort(([, economy1], [, economy2]) => {
      return economy1 - economy2;
    });
  return economiesArray[0];
}
module.exports = bowlerWithBestEconomyInSuperOvers;
