function extraRunsPerTeam(matchesArray, deliveriesArray, year = 2016) {
  const matchesInYear = matchesArray
    .filter((matchesArray) => matchesArray.season == year)
    .map((match) => match.id);

  const extraRunsPerTeaminYear = deliveriesArray.reduce((acc, curr) => {
    if (matchesInYear.includes(curr.match_id)) {
      if (acc[curr.bowling_team] === undefined) {
        acc[curr.bowling_team] = parseInt(curr.extra_runs);
      } else {
        acc[curr.bowling_team] += parseInt(curr.extra_runs);
      }
    }
    return acc;
  }, {});
  return extraRunsPerTeaminYear;
}
module.exports = extraRunsPerTeam;
