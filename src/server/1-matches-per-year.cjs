function matchesPerYear(matchesArray) {
    let matches = matchesArray.reduce((acc, curr) => {
        if (acc[curr.season]) {
            acc[curr.season]++;
        } else {
            acc[curr.season] = 1;
        }
        return acc;
    }, {})
    return matches;
}
module.exports = matchesPerYear;