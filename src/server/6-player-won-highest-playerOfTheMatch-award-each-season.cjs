function playerWonHighestPalyerofthematchAwardEachSeason(matchesArray) {
  let playerWon = matchesArray.reduce((acc, curr) => {
    let year = curr.season;
    let player = curr.player_of_match;
    if (acc[year] === undefined) {
      acc[year] = { [player]: 1 };
    } else {
      if (acc[year][player] === undefined) {
        acc[year][player] = 1;
      } else {
        acc[year][player] += 1;
      }
    }
    return acc;
  }, {});
  let playerwithHighestPOM = [];
  let players = Object.entries(playerWon);
  players.reduce((acc, curr) => {
    if (acc[curr[0]] === undefined) {
      let sort = Object.entries(curr[1]).sort(([, name1], [, name2]) => {
        return name2 - name1;
      });
      playerwithHighestPOM.push(sort[0]);
    }
    return acc;
  }, {});
  return Object.fromEntries(playerwithHighestPOM);
}
module.exports = playerWonHighestPalyerofthematchAwardEachSeason;
