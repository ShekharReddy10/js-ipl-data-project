function strikeRateOfBatsman(matchesArray, deliveriesArray) {
  let matchIDS = matchesArray.reduce((acc, curr) => {
    if (acc[curr.id] === undefined) {
      acc[curr.id] = curr.season;
    }
    return acc;
  });
  let playerRunsAndBalls = deliveriesArray.reduce((acc, curr) => {
    let runs = curr.batsman_runs;
    let ballCount = 1;
    let batsman = curr.batsman;
    let year = matchIDS[curr.match_id];
    if (curr.wide_runs !== "0" || curr.noball_runs !== "0") {
      ballCount = 0;
    }
    if (acc[batsman] === undefined) {
      acc[batsman] = { [year]: [parseInt(runs), ballCount] };
    } else {
      if (acc[batsman][year] === undefined) {
        acc[batsman][year] = [parseInt(runs), ballCount];
      } else {
        let addRunsAndBalls = acc[batsman][year];
        acc[batsman][year] = [
          addRunsAndBalls[0] + parseInt(runs),
          addRunsAndBalls[1] + ballCount,
        ];
      }
    }
    return acc;
  });
  //console.log(strikeRate);
  const strikeRate = Object.entries(playerRunsAndBalls).reduce((acc, curr) => {
    if (acc[curr[0]] === undefined) {
      let runsAndBallsArray = Object.entries(curr[1]);
      let strike = Object.fromEntries(
        runsAndBallsArray.map((rate) => {
          rate[1] = ((rate[1][0] / rate[1][1]) * 100).toFixed(2);
          return [rate[0], rate[1]];
        })
      );
      acc[curr[0]] = strike;
    }
    return acc;
  }, {});
  return strikeRate
}
module.exports = strikeRateOfBatsman;
