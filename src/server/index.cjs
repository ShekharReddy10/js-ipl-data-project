const csvToJson = require("csvtojson");
const fs = require("fs");
const path = require("path");
const matchesCSV = path.join(__dirname, "../data/matches.csv");
const deliveriesCSV = path.join(__dirname, "../data/deliveries.csv");

const matchesPerYear = require(path.join(__dirname, "1-matches-per-year.cjs"));
const matchesWonPerTeamPerYear = require(path.join(
  __dirname,
  "2-matches-won-per-team-per-year.cjs"
));
const extraRunsPerTeam = require(path.join(
  __dirname,
  "3-extra-runs-conceded-per-team-2016.cjs"
));
const top10EconomicalBowlers = require(path.join(
  __dirname,
  "4-top-10-economical-bowlers-2015.cjs"
));
const teamWonTossAndMatch = require(path.join(
  __dirname,
  "5-team-won-toss-and-match.cjs"
));
const playerWonHighestPalyerofthematchAwardEachSeason = require(path.join(
  __dirname,
  "6-player-won-highest-playerOfTheMatch-award-each-season.cjs"
));
const strikeRateOfBatsman = require(path.join(
  __dirname,
  "7-strike-rate-of-batsman-each-season.cjs"
));
const HighestOnePlayerDismissedByAnotherPlayer = require(path.join(
  __dirname,
  "8-highest-onePlayer-dismissed-by-anotherPlayer.cjs"
));
const bowlerWithBestEconomyInSuperOvers = require(path.join(
  __dirname,
  "9-bowler-with-bestEconomy-in-superovers.cjs"
));

csvToJson()
  .fromFile(matchesCSV)
  .then((matchesArray) => {
    csvToJson()
      .fromFile(deliveriesCSV)
      .then((deliveriesArray) => {
        const resultMatchesPerYear = matchesPerYear(matchesArray);
        fs.writeFileSync(
          path.join(__dirname, "../public/output/matchesPerYear.json"),
          JSON.stringify(resultMatchesPerYear)
        );
        const resultMatchesWonPerTeamPerYear =
          matchesWonPerTeamPerYear(matchesArray);
        fs.writeFileSync(
          path.join(
            __dirname,
            "../public/output/matchesWonPerTeamPerYear.json"
          ),
          JSON.stringify(resultMatchesWonPerTeamPerYear)
        );
        const resultextraRunsPerTeam = extraRunsPerTeam(
          matchesArray,
          deliveriesArray
        );
        fs.writeFileSync(
          path.join(
            __dirname,
            "../public/output/extraRunsConcededPerTeam2016.json"
          ),
          JSON.stringify(resultextraRunsPerTeam)
        );
        const resultTop10EconomicalBowlers = top10EconomicalBowlers(
          matchesArray,
          deliveriesArray
        );
        fs.writeFileSync(
          path.join(
            __dirname,
            "../public/output/top10EconomicalBowlers2015.json"
          ),
          JSON.stringify(resultTop10EconomicalBowlers)
        );
        const resultTeamWonTossAndMatch = teamWonTossAndMatch(matchesArray);
        fs.writeFileSync(
          path.join(__dirname, "../public/output/teamWonTossAndMatch.json"),
          JSON.stringify(resultTeamWonTossAndMatch)
        );
        const resultPlayerWinHighestPOM =
          playerWonHighestPalyerofthematchAwardEachSeason(matchesArray);
        fs.writeFileSync(
          path.join(
            __dirname,
            "../public/output/playerWonHighestPlayerofthematchEachSeason.json"
          ),
          JSON.stringify(resultPlayerWinHighestPOM)
        );
        const resultStrikeRateOfBatsman = strikeRateOfBatsman(
          matchesArray,
          deliveriesArray
        );
        fs.writeFileSync(
          path.join(
            __dirname,
            "../public/output/strikeRateOfBatsmanEachSeason.json"
          ),
          JSON.stringify(resultStrikeRateOfBatsman)
        );
        const resultHighestOnePlayerDismissedByAnotherPlayer =
          HighestOnePlayerDismissedByAnotherPlayer(deliveriesArray);
        fs.writeFileSync(
          path.join(
            __dirname,
            "../public/output/highestOnePlayerDissmissedByAnotherPlayer.json"
          ),
          JSON.stringify(resultHighestOnePlayerDismissedByAnotherPlayer)
        );
        const resultBowlerWithBEstEconomyInSuperOvers =
          bowlerWithBestEconomyInSuperOvers(deliveriesArray);
        fs.writeFileSync(
          path.join(
            __dirname,
            "../public/output/bowlerWithBestEconomicalInSuperOvers.json"
          ),
          JSON.stringify(resultBowlerWithBEstEconomyInSuperOvers)
        );
      });
  });
