function top10EconomicalBowlers(matchesArray, deliveriesArray, year = 2015) {
  const matchesInYear = matchesArray
    .filter((matchesArray) => {
      return matchesArray.season == year;
    })
    .map((match) => {
      return match.id;
    });
  const deliveriesInYear = deliveriesArray.filter((delivery) => {
    return matchesInYear.includes(delivery.match_id);
  });
  const runsAndBalls = deliveriesInYear.reduce((acc, curr) => {
    let ballCount = 1;
    let bowler = curr.bowler;
    if (curr.wide_runs !== "0" || curr.noball_runs !== "0") {
      ballCount = 0;
    }
    if (acc[bowler] === undefined) {
      acc[bowler] = [parseInt(curr.total_runs), ballCount];
    } else {
      let addRunsAndBalls = acc[bowler];
      acc[bowler] = [
        addRunsAndBalls[0] + parseInt(curr.total_runs),
        addRunsAndBalls[1] + ballCount,
      ];
    }
    return acc;
  }, {});
  const economiesArray = Object.entries(runsAndBalls)
    .map((bowler) => {
      bowler[1] = parseFloat(((bowler[1][0] * 6) / bowler[1][1]).toFixed(2));
      return [bowler[0], bowler[1]];
    })
    .sort(([, economy1], [, economy2]) => {
      return economy1 - economy2;
    });
  const top10Economies = Object.fromEntries(economiesArray.slice(0, 10));
  return top10Economies;
}
module.exports = top10EconomicalBowlers;
