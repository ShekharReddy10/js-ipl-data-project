function teamWonTossAndMatch(matchesArray) {
  const tossAndWon = matchesArray.reduce((acc, curr) => {
    let winner = curr.winner;
    if (curr.toss_winner === winner) {
      if (acc[winner] === undefined) {
        acc[winner] = 1;
      } else {
        acc[winner] += 1;
      }
    }
    return acc;
  }, {});
  return tossAndWon;
}
module.exports = teamWonTossAndMatch;
